package com.example.redisgeo.service;

import java.util.Collection;
import java.util.List;

import org.springframework.data.geo.Circle;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Metric;
import org.springframework.data.geo.Point;
import org.springframework.data.redis.connection.RedisGeoCommands;

import com.example.redisgeo.bean.CityInfo;

public interface IGeoService {
	
	/**
	 * @Title: saveCityInfoToRedis
	 * @Description:  把城市信息保存到 Redis 中
	 * @Author 刘 仁
	 * @DateTime 2020年7月17日 下午3:45:30
	 * @param cityInfos
	 * @return 成功保存的个数
	 */
	Long saveCityInfoToRedis(Collection<CityInfo> cityInfos);
	
	/**
	 * @Title: getCityPos
	 * @Description: 获取给定城市的坐标
	 * @Author 刘 仁
	 * @DateTime 2020年7月17日 下午3:46:22
	 * @param  cities 给定城市 key
	 * @return
	 */
	List<Point> getCityPos(String[] cities);
	
	/**
	 * @Title: getTwoCityDistance
	 * @Description: 获取两个城市之间的距离
	 * @Author 刘 仁
	 * @DateTime 2020年7月17日 下午3:46:47
	 * @param city1 第一个城市
	 * @param city2 第二个城市
	 * @param metric 单位信息, 可以是 null
	 * @return
	 */
	Distance getTwoCityDistance(String city1, String city2, Metric metric);
	
	/**
	 * @Title: getPointRadius
	 * @Description: 根据给定地理位置坐标获取指定范围内的地理位置集合
	 * @Author 刘 仁
	 * @DateTime 2020年7月17日 下午3:47:38
	 * @param within 中心点和距离
	 * @param args 限制返回的个数和排序方式, 可以是 null
	 * @return
	 */
	GeoResults<RedisGeoCommands.GeoLocation<String>> getPointRadius(
            Circle within, RedisGeoCommands.GeoRadiusCommandArgs args);
	
	/**
	 * @Title: getMemberRadius
	 * @Description: 根据给定地理位置获取指定范围内的地理位置集合
	 * @Author 刘 仁
	 * @DateTime 2020年7月17日 下午3:48:12
	 * @param member
	 * @param distance
	 * @param args
	 * @return
	 */
	GeoResults<RedisGeoCommands.GeoLocation<String>> getMemberRadius(
            String member, Distance distance, RedisGeoCommands.GeoRadiusCommandArgs args);

	/**
	 * @Title: getCityGeoHash
	 * @Description: 获取某个地理位置的 geohash 值
	 * @Author 刘 仁
	 * @DateTime 2020年7月17日 下午3:48:29
	 * @param cities 给定城市 key
	 * @return
	 */
	List<String> getCityGeoHash(String[] cities);
}
